import requests
import json
import threading
import uuid
from uuid import UUID

batch_id = None

class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return obj.hex
        return json.JSONEncoder.default(self, obj)

class Payload(object):
    def __init__(self, j):
        self.__dict__ = json.loads(j)
        
class Exchange:
    def __init__(self, batch_id, exchange_rate):
        self.batch_id = batch_id
        self.exchange_rate = exchange_rate

class Coin:
    def __init__(self, tag, 
                 algorithm, 
                 block_time, 
                 block_reward, 
                 block_reward24, 
                 difficulty, 
                 exchange_rate, 
                 exchange_rate24, 
                 exchange_rate_curr, 
                 btc_revenue, 
                 btc_revenue24,
                 last_block,
                 difficulty24,
                 nethash,
                 market_cap,
                 estimated_rewards,
                 estimated_rewards24,
                 profitability,
                 profitability24,
                 lagging,
                 batch_id):
        self.tag = tag
        self.algorithm = algorithm
        self.block_time = block_time
        self.block_reward = block_reward
        self.block_reward24 = block_reward24
        self.difficulty = difficulty
        self.exchange_rate = exchange_rate
        self.exchange_rate24 = exchange_rate24
        self.exchange_rate_curr = exchange_rate_curr
        self.btc_revenue = btc_revenue
        self.btc_revenue24 = btc_revenue24
        self.last_block = last_block
        self.difficulty24 = difficulty24
        self.nethash = nethash
        self.market_cap = market_cap
        self.estimated_rewards = estimated_rewards
        self.estimated_rewards24 = estimated_rewards24
        self.profitability = profitability
        self.profitability24 = profitability24
        self.lagging = lagging
        self.batch_id = batch_id

def get_coins():
    global batch_id
    batch_id = uuid.uuid4()
    coin_objects = []
    r = requests.get("http://whattomine.com/coins.json")

    coins = Payload(r.content)

    for coin in coins.coins:
        coin_reference = coins.coins[coin]
        if coin_reference["algorithm"] != "ZHash" and coin_reference["algorithm"] != "PHI2" and coin_reference["algorithm"] != "Xevan" and coin_reference["algorithm"] != "PHI1612":
            coin_object = Coin(coin_reference["tag"], 
                               coin_reference["algorithm"], 
                               coin_reference["block_time"], 
                               coin_reference["block_reward"], 
                               coin_reference["block_reward24"], 
                               coin_reference["difficulty"], 
                               coin_reference["exchange_rate"], 
                               coin_reference["exchange_rate24"], 
                               coin_reference["exchange_rate_curr"], 
                               coin_reference["btc_revenue"], 
                               coin_reference["btc_revenue24"],
                               coin_reference["last_block"],
                               coin_reference["difficulty24"],
                               coin_reference["nethash"],
                               coin_reference["market_cap"],
                               coin_reference["estimated_rewards"],
                               coin_reference["estimated_rewards24"],
                               coin_reference["profitability"],
                               coin_reference["profitability24"],
                               coin_reference["lagging"],
                               batch_id)
            coin_objects.append(coin_object)

    data = json.dumps([ob.__dict__ for ob in coin_objects], cls=UUIDEncoder)
    r = requests.post("http://127.0.0.1:5000/cryptometadata", json=data)

    print("Coin job complete")
    
def get_exchange():
    r = requests.get("https://blockchain.info/ticker")
    
    exchange = Payload(r.content)
    
    exchange_object = Exchange(batch_id, exchange.USD['last'])
    
    data = json.dumps(exchange_object.__dict__, cls=UUIDEncoder)
    r = requests.post("http://127.0.0.1:5000/exchangemetadata", json=data)
    
    print("Exchange job complete")

def coin_job():
    threading.Timer(60.0, coin_job).start()
    get_coins()
    get_exchange()

coin_job()

    


