from flask import Flask, jsonify, request, abort, send_file, Blueprint
from flask_pymongo import PyMongo
from pprint import pprint
from io import BytesIO
from datetime import datetime
import json
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from . import data_storage, mongo

@data_storage.route('/coins/populateused', methods=['GET'])
def populate_coins_used():
    cryptometadata = mongo.db.cryptometadata
    usedcoins = mongo.db.usedcoins
    
    iteration = 0
    limit = 10
    coin_list = []
    
    for q in cryptometadata.distinct('tag'):
        if iteration == limit:
            break
        else:
            iteration += 1
        coin_list.append(q)
    
    usedcoins.insert({
        "list": coin_list        
    })
    
    return "Done"
    
@data_storage.route('/gpumetadata/gpus/populate', methods=['GET'])
def populate_gpus():
    mongo.db.drop_collection("gpumetadata")
    gpumetadata = mongo.db.gpumetadata
    df = pd.read_csv('resources/GraphicsCardHashRates.csv')   
    for index, row in df.iterrows():
        gpumetadata.insert({
            'manufacturer' : row['Manufacturer'], 
            'name' : row['Name'], 
            'algorithm' : row['Algorithm'], 
            'hashrate' : row['Hashrate'],  
            'hash_unit' : row['Hash unit'], 
            'power_consumption' : row['Power Consumption']
        })
    
    return "GPU Metadata successfully added"

@data_storage.route('/cryptometadata/<tag>', methods=['GET'])
def get_cryptometadata_by_tag(tag):
    cryptometadata = mongo.db.cryptometadata

    q = cryptometadata.find_one({"tag" : tag})
    if q:
        result = {
                'tag' : q['tag'], 
                'algorithm' : q['algorithm'], 
                'block_time' : q['block_time'], 
                'block_reward' : q['block_reward'],  
                'block_reward24' : q['block_reward24'], 
                'difficulty' : q['difficulty'], 
                'exchange_rate' : q['exchange_rate'], 
                'exchange_rate24' : q['exchange_rate24'],
                'exchange_rate_curr' : q['exchange_rate_curr'],
                'btc_revenue' : q['btc_revenue'],
                'btc_revenue24' : q['btc_revenue24'],
                'last_block' : q['last_block'],
                'difficulty24' : q['difficulty24'],
                'nethash' : q['nethash'],
                'market_cap' : q['market_cap'],
                'estimated_rewards' : q['estimated_rewards'],
                'estimated_rewards24' : q['estimated_rewards24'],
                'profitability' : q['profitability'],
                'profitability24' : q['profitability24'],
                'lagging' : q['lagging'],
                'batch_id' : q['batch_id'],
                'date_time_captured' : q['date_time_captured']
            }

        return jsonify(result)
    else:
        abort(404) 

@data_storage.route('/cryptometadata', methods=['POST'])
def add_cryptometadata():
    cryptometadata = mongo.db.cryptometadata
    batches = mongo.db.batches

    coins = json.loads(request.json)

    for coin in coins:
        cryptometadata.insert({
            'tag' : coin['tag'], 
            'algorithm' : coin['algorithm'], 
            'block_time' : coin['block_time'], 
            'block_reward' : coin['block_reward'],  
            'block_reward24' : coin['block_reward24'], 
            'difficulty' : coin['difficulty'], 
            'exchange_rate' : coin['exchange_rate'], 
            'exchange_rate24' : coin['exchange_rate24'],
            'exchange_rate_curr' : coin['exchange_rate_curr'],
            'btc_revenue' : coin['btc_revenue'],
            'btc_revenue24' : coin['btc_revenue24'],
            'last_block' : coin['last_block'],
            'difficulty24' : coin['difficulty24'],
            'nethash' : coin['nethash'],
            'market_cap' : coin['market_cap'],
            'estimated_rewards' : coin['estimated_rewards'],
            'estimated_rewards24' : coin['estimated_rewards24'],
            'profitability' : coin['profitability'],
            'profitability24' : coin['profitability24'],
            'lagging' : coin['lagging'],
            'batch_id' : coin['batch_id'],
            'date_time_captured': datetime.now()
        })
            
    for coin in coins:
        batches.insert({
            'batch_id' : coin['batch_id'],
            'date_time_captured': datetime.now()
        })
        break

    return "Crypto data successfully added"

@data_storage.route('/exchangemetadata', methods=['POST'])
def add_exchangemetadata():
    exchangemetadata = mongo.db.exchangemetadata
    
    exchange = json.loads(request.json)
    
    exchangemetadata.insert({
            'batch_id': exchange['batch_id'],
            'exchange_rate': exchange['exchange_rate']
            })