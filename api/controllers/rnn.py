from flask import Flask, jsonify, request, abort, send_file, Blueprint
from uuid import UUID
import gc

from io import BytesIO
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from datetime import datetime
from keras import backend as K
from keras.models import load_model
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, CuDNNLSTM
from keras.layers import Dropout
from keras import optimizers
from matplotlib.ticker import MaxNLocator
from keras.utils import plot_model

from . import rnn, mongo

@rnn.route('/rnn/predict/all/<metric>/<coin>/<date_time>', methods=['GET'])
def predict_metric_for_coin_all(metric, coin, date_time):
    timestamps = 60
    X_train, y_train, training_set, sc = get_prediction_history_set(date_time, metric, coin, timestamps)
    test_set = get_prediction_set(metric, coin, date_time)
    
    regressor = load_model(coin + '_' + metric + '_model.h5')
    
    dataset_total = np.concatenate((training_set, test_set), axis=0)
    inputs = dataset_total[len(dataset_total) - len(test_set) - timestamps:]
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)
    X_test = []
    for i in range(timestamps, timestamps + len(test_set)):
        X_test.append(inputs[i-timestamps:i, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    
    prediction = regressor.predict(X_test, batch_size=1000)
    prediction = sc.inverse_transform(prediction)
    prediction = (prediction.ravel()).tolist()
    
    del regressor
    gc.collect()
    K.clear_session()
    
    return prediction

@rnn.route('/rnn/predict/<metric>/<coin>/<date_time>/<timesteps>', methods=['GET'])
def predict_metric_for_coin(metric, coin, date_time, timesteps):
    timestamps = 60
    X_train, y_train, training_set, sc = get_prediction_history_set(date_time, metric, coin, timestamps)
    test_set = get_prediction_set(metric, coin, date_time, timesteps)
    
    regressor = load_model(coin + '_' + metric + '_model2.h5')
    
    dataset_total = np.concatenate((training_set, test_set), axis=0)
    inputs = dataset_total[len(dataset_total) - len(test_set) - timestamps:]
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)
    X_test = []
    for i in range(timestamps, timestamps + len(test_set)):
        X_test.append(inputs[i-timestamps:i, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    
    prediction = regressor.predict(X_test, batch_size=1000)
    prediction = sc.inverse_transform(prediction)
    prediction = (prediction.ravel()).tolist()
    
    del regressor
    gc.collect()
    K.clear_session()
    
    return sum(prediction) / float(len(prediction))
    
@rnn.route('/rnn/initial/<metric>', methods=['GET'])
def initial_train(metric):
    usedcoins = mongo.db.usedcoins
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    
    for coin in coin_list:
        timestamps = 60
        neurons = 200
        dropout = 0
        adam = optimizers.Adam(lr=0.01)
        loss = 'mean_squared_error'
        epochs = 100
        batch_size = 1000
        X_train, y_train, training_set, sc = get_train_set(metric, coin, timestamps)
        
        if X_train == [] or y_train == [] or training_set == []:
            continue
        
        regressor = Sequential()
        # Input layer
        regressor.add(CuDNNLSTM(units=neurons, return_sequences=True, input_shape=(X_train.shape[1], 1)))
        regressor.add(Dropout(dropout))
        
        regressor.add(CuDNNLSTM(units=neurons))
        regressor.add(Dropout(dropout))
        # Output layer
        regressor.add(Dense(units=1))
    
        #
        # RNN compilation
        #
        regressor.compile(optimizer=adam, loss=loss)
    
        #
        # RNN fitting
        #
        regressor_history = regressor.fit(X_train, y_train, epochs=epochs, batch_size=batch_size)
        
        regressor.save(coin + '_' + metric +'_model2.h5')
        
        del regressor
        
@rnn.route('/rnn/initial/exchange_rate/done', methods=['GET'])
def exchange_rate_initial_train():
    usedcoins = mongo.db.usedcoins
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    
    for coin in coin_list:
        timestamps = 60
        neurons = 150
        dropout = 0
        adam = optimizers.Adam(lr=0.01)
        loss = 'mean_squared_error'
        epochs = 50
        batch_size = 1000
        X_train, y_train, training_set, sc = get_train_set("exchange_rate", q, timestamps)
        
        if X_train == [] or y_train == [] or training_set == []:
            continue
        
        regressor = Sequential()
        # Input layer
        regressor.add(CuDNNLSTM(units=neurons, return_sequences=True, input_shape=(X_train.shape[1], 1)))
        regressor.add(Dropout(dropout))
        
        regressor.add(CuDNNLSTM(units=neurons))
        regressor.add(Dropout(dropout))
        # Output layer
        regressor.add(Dense(units=1))
    
        #
        # RNN compilation
        #
        regressor.compile(optimizer=adam, loss=loss)
    
        #
        # RNN fitting
        #
        regressor_history = regressor.fit(X_train, y_train, epochs=epochs, batch_size=batch_size)
        
        regressor.save(q + '_' + "exchange_rate" +'_model.h5')
        
        del regressor

@rnn.route('/rnn/tuning/epochs', methods=['GET'])
def epoch_tuning():
    usedcoins = mongo.db.usedcoins
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    metric_list = ["difficulty", "exchange_rate"]
    average_epoch_history = []
    epoch_history = []
    
    for metric in metric_list:
        epoch_history = []
        for coin in coin_list:
            timestamps = 60
            neurons = 200
            dropout = 0
            adam = optimizers.Adam(lr=0.01)
            loss = 'mean_squared_error'
            epochs = 150
            batch_size = 1000
            X_train, y_train, training_set, sc = get_train_set(metric, coin, timestamps)
        

            regressor = Sequential()
            regressor.add(CuDNNLSTM(units=neurons, return_sequences=True, input_shape=(X_train.shape[1], 1)))
            regressor.add(Dropout(dropout))
            
            regressor.add(CuDNNLSTM(units=neurons))
            regressor.add(Dropout(dropout))

            regressor.add(Dense(units=1))
        
            regressor.compile(optimizer=adam, loss=loss)
        
            regressor_history = regressor.fit(X_train, y_train, epochs=epochs, batch_size=batch_size)
            epoch_history.append(regressor_history.history['loss'])
    
            print(coin + " done")
            del regressor
            K.clear_session()
    
        averaged_epoch_history = np.array(epoch_history)
        averaged_epoch_history = np.average(averaged_epoch_history, axis=0)
        average_epoch_history.append(averaged_epoch_history)
        
    fig = plt.figure(figsize=(8, 4.5), dpi=150)
    ax = fig.gca()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    for i in range(len(average_epoch_history)):
        plt.plot(average_epoch_history[i])
        ax.lines[i].set_label(str(metric_list[i]))
    
    plt.xlabel('Number of epochs')
    plt.ylabel('loss (MSE)')
    plt.legend()
    
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    plt.close(fig)
    return send_file(img, mimetype='image/png')

@rnn.route('/rnn/tuning/<metric>/<test_name>', methods=['GET'])
def rnn_tuning(metric, test_name):
    usedcoins = mongo.db.usedcoins
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    neuron_list = [50, 100, 200]
    average_epoch_history = []
    epoch_history = []
    
    for neuron in neuron_list:
        epoch_history = []
        for coin in coin_list:
            timestamps = 60
            neurons = neuron
            dropout = 0
            adam = optimizers.Adam(lr=0.01)
            loss = 'mean_squared_error'
            epochs = 50
            batch_size = 1000
            X_train, y_train, training_set, sc = get_train_set(metric, coin, timestamps)
        
            # 
            # RNN structure
            #
            regressor = Sequential()
            # Input layer
            regressor.add(CuDNNLSTM(units=neurons, return_sequences=True, input_shape=(X_train.shape[1], 1)))
            regressor.add(Dropout(dropout))
            
            regressor.add(CuDNNLSTM(units=neurons))
            regressor.add(Dropout(dropout))
            # Output layer
            regressor.add(Dense(units=1))
        
            #
            # RNN compilation
            #
            regressor.compile(optimizer=adam, loss=loss)
        
            #
            # RNN fitting
            #
            regressor_history = regressor.fit(X_train, y_train, epochs=epochs, batch_size=batch_size)
            epoch_history.append(regressor_history.history['loss'])
    
            print(coin + " done")
            del regressor
            K.clear_session()
    
        averaged_epoch_history = np.array(epoch_history)
        averaged_epoch_history = np.average(averaged_epoch_history, axis=0)
        average_epoch_history.append(averaged_epoch_history)
        
    fig = plt.figure(figsize=(8, 4.5), dpi=150)
    ax = fig.gca()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    for i in range(len(average_epoch_history)):
        print(str(average_epoch_history[i][-1]) + "from " + str(neuron_list[i]) + " neurons")
        plt.plot(average_epoch_history[i])
        ax.lines[i].set_label(str(neuron_list[i]) + " neurons")
    
    plt.xlabel('Number of epochs')
    plt.ylabel('loss (MSE)')
    plt.legend()
    
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    plt.close(fig)
    return send_file(img, mimetype='image/png')

def get_test_set(date_time, metric, coin):
    cryptometadata = mongo.db.cryptometadata

    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')

    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]) - 1, int(time[0]), int(time[1]), int(time[2]))
    date_upper_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    results = []

    for q in cryptometadata.find({
        "tag":coin,
        "date_time_captured" : {"$gte": date_lower_range,"$lte": date_upper_range}
    }):
        results.append(q[metric])

    test_set = np.array(results)
    test_set = test_set.reshape(-1, 1)
    
    return test_set

def get_prediction_set(metric, coin, date_time, timesteps):
    cryptometadata = mongo.db.cryptometadata

    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')

    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    results = []

    for q in cryptometadata.find({
        "tag":coin,
        "date_time_captured" : {"$gte": date_lower_range}
    }).limit(timesteps):
        results.append(q[metric])

    test_set = np.array(results)
    test_set = test_set.reshape(-1, 1)
    
    return test_set

def get_prediction_history_set(date_time, metric, coin, timestamps):
    cryptometadata = mongo.db.cryptometadata

    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')

    date_lower_range = datetime(2018, 9, 26)
    date_upper_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    results = []

    for q in cryptometadata.find({
        "tag":coin,
        "date_time_captured" : {"$gte": date_lower_range,"$lte": date_upper_range}
    }):
        results.append(q[metric])
            
    if results == []:
        return [], [], []

    training_set = np.array(results)
    training_set = training_set.reshape(-1, 1)

    # Preprocessing
    sc = StandardScaler()
    training_set_scaled = sc.fit_transform(training_set)

    # Timestamps
    X_train = []
    y_train = []
    for i in range(timestamps, len(training_set)-1):
        X_train.append(training_set_scaled[i-timestamps:i, 0])
        y_train.append(training_set_scaled[i, 0])
    X_train, y_train = np.array(X_train), np.array(y_train)

    # Reshaping
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

    return (X_train, y_train, training_set, sc)
    
def get_train_set(metric, coin, timestamps):
    cryptometadata = mongo.db.cryptometadata

    date_lower_range = datetime(2018, 9, 26)

    results = []

    for q in cryptometadata.find({
        "$and":[ {"tag":coin}, {"date_time_captured" : {"$gte": date_lower_range}}]
    }):
        if metric in q:
            results.append(q[metric])
            
    if results == []:
        return [], [], []

    training_set = np.array(results)
    training_set = training_set.reshape(-1, 1)

    # Preprocessing
    sc = StandardScaler()
    training_set_scaled = sc.fit_transform(training_set)

    # Timestamps
    X_train = []
    y_train = []
    for i in range(timestamps, len(training_set)-1):
        X_train.append(training_set_scaled[i-timestamps:i, 0])
        y_train.append(training_set_scaled[i, 0])
    X_train, y_train = np.array(X_train), np.array(y_train)

    # Reshaping
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

    return (X_train, y_train, training_set, sc)

