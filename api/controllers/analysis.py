from flask import Flask, jsonify, request, abort, send_file, Blueprint
from flask_pymongo import PyMongo
from pprint import pprint
from io import BytesIO
from datetime import datetime
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
from uuid import UUID
from bson.objectid import ObjectId
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.offline as offline
import matplotlib.dates as mdates

from . import crypto_analysis, mongo

plotly.tools.set_credentials_file(username='chrisdreyer', api_key='fFcSBSRYF198y3kkazko')

@crypto_analysis.route('/analysis/pso/profitabilitypergpu/<variant>', methods=['GET'])
def pso_profitability_per_gpu(variant):
    psoresults = mongo.db.psoresults2
    wtm_gtx_1080 = []
    wtm_rx_580 = []
    wtm_rx_480 = []
    model_gtx_1080 = []
    model_rx_580 = []
    model_rx_480 = []
    wtm_results = []
    model_results = []
    
    for q in psoresults.find({'variant':variant}):
        if q['timesteps'] == 240:
            if q['gpu'] == 'gtx_1080':
                wtm_gtx_1080.append(q['wtm_mining_result'])
                model_gtx_1080.append(q['model_mining_result'])
            elif q['gpu'] == 'rx_580':
                wtm_rx_580.append(q['wtm_mining_result'])
                model_rx_580.append(q['model_mining_result'])
            elif q['gpu'] == 'rx_480':
                wtm_rx_480.append(q['wtm_mining_result'])
                model_rx_480.append(q['model_mining_result'])
          
    wtm_gtx_1080_value = sum(wtm_gtx_1080) / float(len(wtm_gtx_1080))
    wtm_rx_580_value = sum(wtm_rx_580) / float(len(wtm_rx_580))
    wtm_rx_480_value = sum(wtm_rx_480) / float(len(wtm_rx_480))
    model_gtx_1080_value = sum(model_gtx_1080) / float(len(model_gtx_1080))
    model_rx_580_value = sum(model_rx_580) / float(len(model_rx_580))
    model_rx_480_value = sum(model_rx_480) / float(len(model_rx_480))
    
    wtm_results.append(wtm_gtx_1080_value)
    wtm_results.append(wtm_rx_580_value)
    wtm_results.append(wtm_rx_480_value)
    model_results.append(model_gtx_1080_value)
    model_results.append(model_rx_580_value)
    model_results.append(model_rx_480_value)
            
    y_pos = ['GTX 1080', 'RX 580', 'RX 480']
    plt.clf()
    plt.figure(num=None, figsize=(8, 4.5), dpi=150, facecolor='w', edgecolor='k')
    plt.bar(y_pos, model_results, label='New model LSTM predictions without PSO')
    plt.bar(y_pos, wtm_results, label='WhatToMine')
    plt.xlabel('GPU')
    plt.ylabel('Profitability')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)

    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    plt.close()
    return send_file(img, mimetype='image/png')
    
@crypto_analysis.route('/analysis/pso/profitabilityimprovement/<variant>', methods=['GET'])
def pso_profitability_improvement(variant):
    psoresults = mongo.db.psoresults2
    wtm_profitability = 0
    model_profitability = 0
    
    for q in psoresults.find({'variant':variant}):
        if q['timesteps'] == 1440:
            wtm_profitability += q['wtm_mining_result']
            model_profitability += q['model_mining_result']
    
    wtm_profitability = wtm_profitability / 3
    model_profitability = model_profitability / 3
    
    return jsonify("WTM profit: " + str(wtm_profitability) + ", Model profit: " + str(model_profitability) + ", Improvement: " + str(model_profitability-wtm_profitability) + ", Percentage increase: " + str(((model_profitability-wtm_profitability) / wtm_profitability) * 100))

@crypto_analysis.route('/analysis/pso/percentagesperposition/<variant>', methods=['GET'])
def pso_percentages_per_position(variant):
    psoresults = mongo.db.psoresults
    wtm_change_per_position = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    model_change_per_position = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    percentages = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    for q in psoresults.find({'variant':variant}):
        for q2 in q['model_change_per_coin']:
            model_change_per_position[int(q2['original_position'])-1] += q2['change']
        for q3 in q['wtm_change_per_coin']:
            wtm_change_per_position[int(q3['original_position'])-1] += q3['change']
            
    for i in range(10):
        percentages[i] = ((model_change_per_position[i]-wtm_change_per_position[i])/wtm_change_per_position[i]) * 100
        
    return jsonify(percentages)
    
    
@crypto_analysis.route('/analysis/pso/percentages/<variant>', methods=['GET'])
def pso_percentages(variant):
    psoresults = mongo.db.psoresults2
    better = 0
    same = 0
    worse = 0
    
    for q in psoresults.find({'variant':variant}):
        if q['timesteps'] == 1440:
            if q['wtm_mining_result'] < q['model_mining_result']:
                better += 1
            elif q['wtm_mining_result'] == q['model_mining_result']:
                same += 1
            else:
                worse += 1
            
    total = better + worse + same
    better = (better / total) * 100
    worse = (worse / total) * 100
    same = (same / total) * 100
            
    return jsonify("Better: " + str(round(better, 2)) + ", Worse: " + str(round(worse, 2)) + ", Same: " + str(round(same, 2)))
    
@crypto_analysis.route('/analysis/pso/changepergpu/<variant>', methods=['GET'])
def pso_change_per_gpu(variant):
    psoresults = mongo.db.psoresults
    gpu_list = []
    wtm_change_per_gpu = []
    model_change_per_gpu = []
    
    for q in psoresults.find({'variant':variant}):
        if q['gpu'] not in gpu_list:
            gpu_list.append(q['gpu'])  
            wtm_change_per_gpu.append(q['wtm_change'])
            model_change_per_gpu.append(q['model_change'])
        else:
            wtm_change_per_gpu[gpu_list.index(q['gpu'])] += q['wtm_change']
            model_change_per_gpu[gpu_list.index(q['gpu'])] += q['model_change']
    
    y_pos = gpu_list
    plt.clf()
    plt.figure(num=None, figsize=(8, 4.5), dpi=150, facecolor='w', edgecolor='k')
    
    plt.bar(y_pos, wtm_change_per_gpu, label='WhatToMine')
    plt.bar(y_pos, model_change_per_gpu, label='New model gbest PSO with LSTM predictions')
    plt.xlabel('Test runs')
    plt.ylabel('Change experienced')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)

    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    plt.close()
    return send_file(img, mimetype='image/png')
    
@crypto_analysis.route('/analysis/pso/positionchanges/<variant>', methods=['GET'])
def pso_position_changes(variant):
    psoresults = mongo.db.psoresults
    wtm_change_per_position = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    model_change_per_position = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    for q in psoresults.find({'variant':variant}):
        for q2 in q['model_change_per_coin']:
            model_change_per_position[int(q2['original_position'])-1] += q2['change']
        for q3 in q['wtm_change_per_coin']:
            wtm_change_per_position[int(q3['original_position'])-1] += q3['change']
    
    y_pos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    plt.clf()
    plt.figure(num=None, figsize=(8, 4.5), dpi=150, facecolor='w', edgecolor='k')
    plt.bar(y_pos, wtm_change_per_position, label='WhatToMine')
    plt.bar(y_pos, model_change_per_position, label='New model gbest PSO with LSTM predictions')
    plt.xticks(np.arange(1, 11, 1.0))
    plt.xlabel('Ranking positions')
    plt.ylabel('Change experienced')
    plt.legend()

    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    plt.close()
    return send_file(img, mimetype='image/png')

@crypto_analysis.route('/analysis/pso/allchangelinechart/<variant>', methods=['GET'])
def pso_all_change_linechart(variant):
    psoresults = mongo.db.psoresults2
    wtm_gtx1080_results = []
    wtm_rx580_results = []
    wtm_rx480_results = []
    model_gtx1080_results = []
    model_rx580_results = []
    model_rx480_results = []
    wtm_results = []
    model_results = []
    
    for q in psoresults.find({'variant':variant}):
        if q['timesteps'] == 1440:
            if q['gpu'] == 'gtx_1080':
                wtm_gtx1080_results.append(q['wtm_mining_result'])
                model_gtx1080_results.append(q['model_mining_result'])
            elif q['gpu'] == 'rx_580':
                wtm_rx580_results.append(q['wtm_mining_result'])
                model_rx580_results.append(q['model_mining_result'])
            elif q['gpu'] == 'rx_480':
                wtm_rx480_results.append(q['wtm_mining_result'])
                model_rx480_results.append(q['model_mining_result'])
          
    
    wtm_results.append(wtm_gtx1080_results)
    wtm_results.append(wtm_rx580_results)
    wtm_results.append(wtm_rx480_results)
    model_results.append(model_gtx1080_results)
    model_results.append(model_rx580_results)
    model_results.append(model_rx480_results)
    
    print(wtm_results)
    print(model_results)
    
    wtm_results = np.array(wtm_results)
    model_results = np.array(model_results)
    wtm_results = np.average(wtm_results, axis=0)
    model_results = np.average(model_results, axis=0)
            
    y_pos = np.arange(len(wtm_results))
    plt.clf()
    plt.figure(num=None, figsize=(8, 4.5), dpi=150, facecolor='w', edgecolor='k')
    plt.bar(y_pos, model_results, label='New model LSTM predictions without PSO')
    plt.bar(y_pos, wtm_results, label='WhatToMine')
    plt.xlabel('24 hour intervals')
    plt.ylabel('Profitability')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)

    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    plt.close()
    return send_file(img, mimetype='image/png')


@crypto_analysis.route('/analysis/rnn/tuning/<batch_id>', methods=['GET'])
def rnn_tuning(batch_id):
    rnntuning = mongo.db.rnntuning
    
    results = []
    
    for q in rnntuning.find({
        "batch": UUID(batch_id),
    }):
        results.append(q['loss'])
    print(results)
        
    average_loss = sum(results) / float(len(results))

    return jsonify(average_loss)

@crypto_analysis.route('/analysis/lifetimeaverage/<metric>/<coin>', methods=['GET'])
def lifetime_average(metric, coin):
    cryptometadata = mongo.db.cryptometadata
    
    results = []
    
    for q in cryptometadata.find({
        "tag": coin
    }):
         if metric in q:
            results.append(q[metric])
            
    print(results)
        
    plt.clf()
    plt.figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')
    plt.plot(results, color='red', label=metric)
    plt.title(metric + ' overall average')
    plt.xlabel('Time')
    plt.ylabel(metric)
    plt.legend()
    
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')
    
@crypto_analysis.route('/analysis/onedaygraph/<metric1>/<metric2>/<coin>/<date>', methods=['GET'])
def get_one_day_graph(metric1, metric2, coin, date):
    cryptometadata = mongo.db.cryptometadata

    date = date.split('-')

    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), 0, 0, 0)
    date_upper_range = datetime(int(date[0]), int(date[1]), int(date[2]) + 1, 0, 0, 0)
    results_metric_one = []
    results_metric_two = []

    for q in cryptometadata.find({
        "tag":coin,
        "date_time_captured" : {"$gte": date_lower_range,"$lte": date_upper_range},
    }):
        if metric1 in q:
            results_metric_one.append(q[metric1])
        if metric2 in q:
            results_metric_two.append(q[metric2])
            
    x = [date_lower_range + dt.timedelta(minutes=i) for i in range(len(results_metric_one))]
    plt.clf()
    plt.figure(num=None, figsize=(10, 4.5), dpi=150, facecolor='w', edgecolor='k')
    plt.subplot(1, 2, 1)
    plt.plot(x, results_metric_one, color='orange')
    plt.title(metric1 + ' 24h difference')
    plt.xlabel('Time')
    plt.ylabel(metric1)
    plt.gcf().autofmt_xdate()
    myFmt = mdates.DateFormatter('%H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    
    plt.subplot(1, 2, 2)
    plt.plot(x, results_metric_two)
    plt.title(metric2 + ' 24h difference')
    plt.xlabel('Time')
    plt.ylabel(metric2)
    plt.gcf().autofmt_xdate()
    myFmt = mdates.DateFormatter('%H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@crypto_analysis.route('/analysis/latestdayanalytics/<metric>', methods=['GET'])
def get_latest_day_analytics(metric):
    cryptometadata = mongo.db.cryptometadata

    variance_per_coin = {}
    variance_list = []
    for last_document in cryptometadata.find({
        "$query": {}, "$orderby": {"$natural" : -1}
    }).limit(1):
        last_date = last_document['date_time_captured']

    last_date = datetime(int(last_date.strftime('%Y')),int(last_date.strftime('%m')),int(last_date.strftime('%d')),0,0,0)

    for q in cryptometadata.find({
        "date_time_captured" : { "$gte" : last_date}
    }):
        if q["tag"] not in variance_per_coin:
            variance_per_coin[q["tag"]] = []
        variance_per_coin[q["tag"]].append(q[metric])

    for v in variance_per_coin:
        variance_list.append(np.var(variance_per_coin[v]))

    print(variance_list)
    plt.plot(variance_list)
    plt.ylabel('some numbers')
    
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')