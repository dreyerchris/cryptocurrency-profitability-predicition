from __future__ import division
from flask import Flask, jsonify, request, abort, send_file, Blueprint
from pyswarm import pso as pyswarm_pso
from bson.objectid import ObjectId
from datetime import datetime
from controllers.rnn import predict_metric_for_coin, predict_metric_for_coin_all
from pyswarms.single.global_best import GlobalBestPSO
from pyswarms.single.local_best import LocalBestPSO
from io import BytesIO
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import pprint
import uuid
from uuid import UUID
import gc

from . import pso, mongo

pso_crypto_data = []
pso_gpu_data = []
hashrate = 0
time = 24 * 60 * 60
block_reward = 0
future_difficulty = 0
difficulty = 0
difficulty24 = 0
usd_exchange_rate = 0
future_exchange_rate = 0
btc_exchange_rate = 0
block_time = 0
nethash = 0
tag = ""

@pso.route('/pso/tuning/<batch_id>/<date_time>/<gpu>', methods=['GET'])
def pso_tuning(batch_id, date_time, gpu, current_batch_id=None):
    global hashrate
    global block_reward
    global difficulty
    global difficulty24
    global btc_exchange_rate
    global nethash
    global block_time
    global usd_exchange_rate
    global exchange_rate
    global future_exchange_rate
    global future_difficulty
    global tag
    
    c1 = 0.1
    c2 = 0.1
    w = 0.4
    n = 15
    
    usedcoins = mongo.db.usedcoins
    
    coin_list = []
    parameters = []
    gbest_results = []
    lbest_results = []
    results = []
    
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    
    generate_pso_test_data(batch_id, gpu, coin_list, date_time)
    set_usd_exchange_rate(batch_id)
    pso_future_data = generate_future_mining_data(coin_list, date_time, pso_gpu_data, usd_exchange_rate)
    pprint.pprint(pso_future_data)
    
    future_difficulty = predict_metric_for_coin("difficulty", 'ETH', date_time, 1440)
    future_exchange_rate = predict_metric_for_coin("exchange_rate", 'ETH', date_time, 1440)
    
    for q in pso_crypto_data:
        if q['tag'] == 'ETH':
            block_reward = q['block_reward']
            difficulty = q['difficulty']
            difficulty24 = q['difficulty24']
            btc_exchange_rate = q['exchange_rate']
            nethash = q['nethash']
            block_time = q['block_time']
            tag = q['tag']
            
            for q2 in pso_gpu_data:
                if q2['algorithm'] == q['algorithm']:
                    hashrate = q2['hashrate']
                    break
                elif q['algorithm'] == 'CryptoNightHeavy' and q2['algorithm'] == 'CNHeavy':
                    hashrate = q2['hashrate']
                    break
                
            adjusted_nethash = nethash * (1 + ((future_difficulty - difficulty)/difficulty))
            
            kwargs = {"er": usd_exchange_rate, 
                      "h": hashrate, 
                      "t": 86400, 
                      "bt": block_time,
                      "ber": btc_exchange_rate,
                      "fber": future_exchange_rate,
                      "br": block_reward,
                      "nh": adjusted_nethash,
                      "d": difficulty,
                      "fd": future_difficulty,
                      "d24": difficulty24}
            
            while c1 <= 2:
                c2 = 0.1
                while c2 <= 2:
                    w = 0.4
                    while w <= 0.9:
                        n = 15
                        while n <= 30:
                            parameters.append('c1 = ' + str(c1) + ', c2 = ' + str(c2) + ', w = ' + str(w))
                            x_max = 10 * np.ones(2)
                            x_min = -10 * np.ones(2)
                            bounds = (x_min, x_max)
                            options = {'c1': c1, 'c2': c2, 'w': w}
                            options2 = {'c1': c1, 'c2': c2, 'w': w, 'k': 3, 'p': 2}
                            optimizer2 = LocalBestPSO(n_particles=n, dimensions=2, options=options2, bounds=bounds)
                            optimizer = GlobalBestPSO(n_particles=n, dimensions=2, options=options, bounds=bounds)
                        
                            cost, pos = optimizer.optimize(profitability_future_objective, 1000, print_step=100, verbose=0, **kwargs)
                            cost2, pos2 = optimizer2.optimize(profitability_future_objective, 1000, print_step=100, verbose=0, **kwargs)
                            
                            gbest_results.append({
                                    'type':'gbest PSO',
                                    'c1':c1,
                                    'c2':c2,
                                    'w':w,
                                    'n':n,
                                    'cost':optimizer.cost_history[-1]
                                })
                            lbest_results.append({
                                    'type':'lbest PSO',
                                    'c1':c1,
                                    'c2':c2,
                                    'w':w,
                                    'n':n,
                                    'cost':optimizer2.cost_history[-1]
                                })
                            
                            optimizer.reset()
                            optimizer2.reset()
                            n = n + 1
                            
                        w = w + 0.1
                        w = round(w, 1)
            
                
                    c2 = c2 + 0.1
                    c2 = round(c2,1)
                c1 = c1 + 0.1
                c1 = round(c1,1)
                    
            break
         
    gbest_results = sorted(gbest_results, key=lambda k: k['cost'], reverse=True)
    lbest_results = sorted(lbest_results, key=lambda k: k['cost'], reverse=True)
    results.append(gbest_results[0])
    results.append(gbest_results[-1])
    results.append(lbest_results[0])
    results.append(lbest_results[-1])
    
    return jsonify(results)

@pso.route('/pso/future/all/withpso/<gpu>/<date_time>', methods=['GET'])
def pso_future_all_with_pso(gpu, date_time):
    cryptometadata = mongo.db.cryptometadata
    current_batch_id = uuid.uuid4()
    
    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')

    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    
    while cryptometadata.find({'tag':'ETH', 'date_time_captured':{'$gte': date_lower_range}}).count() > 0:
        current_batch = cryptometadata.find_one({'date_time_captured':{'$gte': date_lower_range}})
        batch_id = current_batch['batch_id']
        pso_future_with_pso(batch_id, date_time, gpu, current_batch_id)
        
        for q in cryptometadata.find({'tag':'ETH','date_time_captured':{'$gte': date_lower_range}}).skip(240): 
            next_batch_date = str(q['date_time_captured'])
            break
        
        next_batch_date = next_batch_date.split('.')[0]
        date_time = next_batch_date.replace(' ', 'Z')
        
        datesplit = date_time.split('Z')
        date = datesplit[0].split('-')
        time = datesplit[1].split(':')
        date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
        
        print(date_time)
        gc.collect()

@pso.route('/pso/future/all/<gpu>/<date_time>', methods=['GET'])
def pso_future_all(gpu, date_time):
    cryptometadata = mongo.db.cryptometadata
    current_batch_id = uuid.uuid4()
    
    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')

    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    
    while cryptometadata.find({'tag':'ETH', 'date_time_captured':{'$gte': date_lower_range}}).count() > 0:
        current_batch = cryptometadata.find_one({'date_time_captured':{'$gte': date_lower_range}})
        batch_id = current_batch['batch_id']
        pso_future(batch_id, date_time, gpu, current_batch_id)
        
        for q in cryptometadata.find({'tag':'ETH','date_time_captured':{'$gte': date_lower_range}}).skip(1440): 
            next_batch_date = str(q['date_time_captured'])
            break
        
        next_batch_date = next_batch_date.split('.')[0]
        date_time = next_batch_date.replace(' ', 'Z')
        
        datesplit = date_time.split('Z')
        date = datesplit[0].split('-')
        time = datesplit[1].split(':')
        date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
        
        print(date_time)
        gc.collect()
        
@pso.route('/pso/future/withpso/<batch_id>/<date_time>/<gpu>', methods=['GET'])
def pso_future_with_pso(batch_id, date_time, gpu, current_batch_id=None):
    global hashrate
    global block_reward
    global difficulty
    global difficulty24
    global btc_exchange_rate
    global nethash
    global block_time
    global usd_exchange_rate
    global exchange_rate
    global future_exchange_rate
    global future_difficulty
    global tag
    
    usedcoins = mongo.db.usedcoins
    profitability = []
    wtm_profitability = []
    
    coin_list = []
    results = []
    
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    
    generate_pso_test_data(batch_id, gpu, coin_list, date_time)
    set_usd_exchange_rate(batch_id)
    
    for q in pso_crypto_data:
        x_max = 10 * np.ones(2)
        x_min = -10 * np.ones(2)
        bounds = (x_min, x_max)
#        options = {'c1': 0.8, 'c2': 0.4, 'w': 0.4}
        options2 = {'c1': 0.8, 'c2': 0.4, 'w': 0.4, 'k': 3, 'p': 2}
        optimizer2 = LocalBestPSO(n_particles=27, dimensions=2, options=options2, bounds=bounds)
#        optimizer = GlobalBestPSO(n_particles=30, dimensions=2, options=options, bounds=bounds)
    
        block_reward = q['block_reward']
        difficulty = q['difficulty']
        difficulty24 = q['difficulty24']
        btc_exchange_rate = q['exchange_rate']
        nethash = q['nethash']
        block_time = q['block_time']
        tag = q['tag']
        
        
        
        for q2 in pso_gpu_data:
            if q2['algorithm'] == q['algorithm']:
                hashrate = q2['hashrate']
                break
            elif q['algorithm'] == 'CryptoNightHeavy' and q2['algorithm'] == 'CNHeavy':
                hashrate = q2['hashrate']
                break
            
        future_difficulty = predict_metric_for_coin("difficulty", tag, date_time, 240)
        adjusted_nethash = nethash * (1 + ((future_difficulty - difficulty)/difficulty))
        
        kwargs2= {"er": usd_exchange_rate, 
                  "h": hashrate, 
                  "t": 14400, 
                  "bt": block_time,
                  "ber": btc_exchange_rate,
                  "fber": btc_exchange_rate,
                  "br": block_reward,
                  "nh": adjusted_nethash,
                  "d": difficulty,
                  "fd": difficulty,
                  "d24": difficulty24}
        
        kwargs = {"er": usd_exchange_rate, 
                  "h": hashrate, 
                  "t": 14400, 
                  "bt": block_time,
                  "ber": btc_exchange_rate,
                  "fber": predict_metric_for_coin("exchange_rate", tag, date_time, 240),
                  "br": block_reward,
                  "nh": adjusted_nethash,
                  "d": difficulty,
                  "fd": future_difficulty,
                  "d24": difficulty24}
    
        cost, pos = optimizer2.optimize(profitability_future_objective, 1000, print_step=100, verbose=0, **kwargs)
        
        profitability.append({'tag': q['tag'], 'profitability': cost, 'hashrate':hashrate})
        wtm_profitability.append({'tag': q['tag'], 'profitability': profitability_future_result(**kwargs2), 'hashrate':hashrate})
            
        results.append({'tag': q['tag'], 'profitability': cost})
        
        print(tag + "_done")
        gc.collect()
           
    model_mining_result = calculate_mining(profitability, date_time, 240, False)
    wtm_mining_result = calculate_mining(wtm_profitability, date_time, 240)
    save_pso_results2(current_batch_id, model_mining_result, wtm_mining_result, 240, gpu, "lbest_pso", date_time)
    gc.collect()

@pso.route('/pso/future/<batch_id>/<date_time>/<gpu>', methods=['GET'])
def pso_future(batch_id, date_time, gpu, current_batch_id=None):
    global hashrate
    global block_reward
    global difficulty
    global difficulty24
    global btc_exchange_rate
    global nethash
    global block_time
    global usd_exchange_rate
    global exchange_rate
    global future_exchange_rate
    global future_difficulty
    global tag
    
    usedcoins = mongo.db.usedcoins
    profitability = []
    wtm_profitability = []
    
    coin_list = []
    
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    
    generate_pso_test_data(batch_id, gpu, coin_list, date_time)
    set_usd_exchange_rate(batch_id)
    
    for q in pso_crypto_data:
        block_reward = q['block_reward']
        difficulty = q['difficulty']
        difficulty24 = q['difficulty24']
        btc_exchange_rate = q['exchange_rate']
        nethash = q['nethash']
        block_time = q['block_time']
        tag = q['tag']
        
        
        
        for q2 in pso_gpu_data:
            if q2['algorithm'] == q['algorithm']:
                hashrate = q2['hashrate']
                break
            elif q['algorithm'] == 'CryptoNightHeavy' and q2['algorithm'] == 'CNHeavy':
                hashrate = q2['hashrate']
                break
            
        future_difficulty = predict_metric_for_coin("difficulty", tag, date_time, 1440)
        adjusted_nethash = nethash * (1 + ((future_difficulty - difficulty)/difficulty))
        
        kwargs2= {"er": usd_exchange_rate, 
                  "h": hashrate, 
                  "t": 86400, 
                  "bt": block_time,
                  "ber": btc_exchange_rate,
                  "fber": btc_exchange_rate,
                  "br": block_reward,
                  "nh": adjusted_nethash,
                  "d": difficulty,
                  "fd": difficulty,
                  "d24": difficulty24}
        
        kwargs = {"er": usd_exchange_rate, 
                  "h": hashrate, 
                  "t": 86400, 
                  "bt": block_time,
                  "ber": btc_exchange_rate,
                  "fber": predict_metric_for_coin("exchange_rate", tag, date_time, 1440),
                  "br": block_reward,
                  "nh": adjusted_nethash,
                  "d": difficulty,
                  "fd": future_difficulty,
                  "d24": difficulty24}
    
        profitability.append({'tag': q['tag'], 'profitability': profitability_future_result(**kwargs), 'hashrate':hashrate})
        wtm_profitability.append({'tag': q['tag'], 'profitability': profitability_future_result(**kwargs2), 'hashrate':hashrate})
            
        
        print(tag + "_done")
        gc.collect()
        
    model_mining_result = calculate_mining(profitability, date_time, 1440)
    wtm_mining_result = calculate_mining(wtm_profitability, date_time, 1440)
    save_pso_results2(current_batch_id, model_mining_result, wtm_mining_result, 1440, gpu, "no_pso", date_time)
           
    gc.collect()

@pso.route('/pso/future/stepwise/<batch_id>/<date_time>/<gpu>', methods=['GET'])
def pso_future_stepwise(batch_id, date_time, gpu):
    global hashrate
    global block_reward
    global difficulty
    global difficulty24
    global btc_exchange_rate
    global nethash
    global block_time
    global usd_exchange_rate
    global exchange_rate
    global future_exchange_rate
    global future_difficulty
    global tag
    
    usedcoins = mongo.db.usedcoins
    
    coin_list = []
    results = []
    
    for q in usedcoins.find({}).sort([('_id', -1)]):
        coin_list = q['list']
        break
    
    generate_pso_test_data(batch_id, gpu, coin_list, date_time)
    set_usd_exchange_rate(batch_id)
    pso_future_data = generate_future_mining_data(coin_list, date_time, pso_gpu_data, usd_exchange_rate)
    
    for q in pso_crypto_data:
        run_results = 0
        
        block_reward = q['block_reward']
        difficulty = q['difficulty']
        difficulty24 = q['difficulty24']
        btc_exchange_rate = q['exchange_rate']
        nethash = q['nethash']
        block_time = q['block_time']
        tag = q['tag']
        
        
        
        for q2 in pso_gpu_data:
            if q2['algorithm'] == q['algorithm']:
                hashrate = q2['hashrate']
                break
            elif q['algorithm'] == 'CryptoNightHeavy' and q2['algorithm'] == 'CNHeavy':
                hashrate = q2['hashrate']
                break
            
        future_difficulties = predict_metric_for_coin_all("difficulty", tag, date_time)
        future_exchange_rates = predict_metric_for_coin_all("exchange_rate", tag, date_time)
        
        for i in range(1339):
            
            kwargs = {"er": usd_exchange_rate, 
                  "h": hashrate, 
                  "t": 60, 
                  "bt": block_time,
                  "ber": btc_exchange_rate,
                  "fber": future_exchange_rates[i],
                  "br": block_reward,
                  "nh": nethash,
                  "d": difficulty,
                  "fd":future_difficulties[i],
                  "d24": difficulty24}
    
            run_results +=  profitability_future_result(**kwargs)
            
        results.append({'tag': q['tag'], 'profitability': run_results})
        
        print(tag + "_done")
           
    gc.collect()

def profitability_future_objective(x, er, h, t, bt, ber, fber, br, nh, d, fd, d24):
    blocks_pd = t / float(bt)
    net_btc = blocks_pd * ber * br
    f_net_bc = blocks_pd * fber * br
    nethash24 = (nh / d) * d24
    f_nethash24 = (nh / fd) * d24
    return ((net_btc * (x[:,0] * f_net_bc)) * (er / (nethash24 * (x[:,1] * f_nethash24)))) * h

def profitability_future_result(er, h, t, bt, ber, fber, br, nh, d, fd, d24):
    blocks_pd = t / float(bt)
    net_btc = blocks_pd * fber * br
    nethash24 = (nh / fd) * d24
    return (net_btc * (er / nethash24)) * h

def con(x):
    x1 = x[0]
    x2 = x[1]
    return [-(x1 + 0.25)**2 + 0.75*x2]
    
def generate_pso_test_data(batch_id, gpu, coin_list, date_time):
    gpumetadata = mongo.db.gpumetadata
    cryptometadata = mongo.db.cryptometadata
    global pso_crypto_data
    global pso_future_crypto_data
    global pso_gpu_data
    pso_crypto_data = []
    pso_future_crypto_data = []
    pso_gpu_data = []
    hashrate = 0
     
    for q in cryptometadata.find({"tag": {'$in': coin_list}, "batch_id" : batch_id}):
        pso_crypto_data.append({
                'tag' : q['tag'], 
                'algorithm' : q['algorithm'],  
                'block_reward' : q['block_reward'],   
                'difficulty' : q['difficulty'],
                'difficulty24': q['difficulty24'],
                'exchange_rate' : q['exchange_rate'],
                'nethash': q['nethash'],
                'block_time': q['block_time'],
                'profitability': q['profitability'],
                'profitability24': q['profitability24']
            })
        
    for q in gpumetadata.find({"name" : gpu}):
        if q['hash_unit'][0] == "K":
            hashrate = q['hashrate'] * 1000
        elif q['hash_unit'][0] == "M":
            hashrate = q['hashrate'] * 1000000
        else:
            hashrate =  q['hashrate']
        pso_gpu_data.append({
                'hashrate' : hashrate, 
                'power_consumption' : q['power_consumption'],
                'algorithm' : q['algorithm']
            })
    
def set_usd_exchange_rate(batch_id):
    exchangemetadata = mongo.db.exchangemetadata
    global usd_exchange_rate
    
    q = exchangemetadata.find_one({'batch_id': batch_id})
    if q:
        usd_exchange_rate = float(q['exchange_rate'])
        
def calculate_position_change(current, future, reverse=True):
    current = sorted(current, key=lambda k: k['profitability'], reverse=reverse)
    future = sorted(future, key=lambda k: k['profitability'], reverse=True)
    
    current_positions = []
    future_positions = []
    total_change = 0
    
    position = 1
    
    for q in current:
        current_positions.append({
            'tag': q['tag'],
            'position': position
        })
        position += 1
        
    position = 1

    for q in future:
        future_positions.append({
            'tag': q['tag'],
            'position': position
        })
        position += 1
    
    for q in current_positions:
        for q2 in future_positions:
            if q2['tag'] == q['tag']:
                total_change += abs(q['position'] - q2['position'])
    pprint.pprint(current_positions)
    pprint.pprint(future_positions)
    return total_change

def calculate_change_per_coin(current_data, pso_future_data):
    current = sorted(current_data, key=lambda k: k['profitability'], reverse=True)
    future = sorted(pso_future_data, key=lambda k: k['profitability'], reverse=True)
    
    current_positions = []
    future_positions = []
    change_per_coin = []
    
    position = 1
    
    for q in current:
        current_positions.append({
            'tag': q['tag'],
            'position': position
        })
        position += 1
        
    position = 1

    for q in future:
        future_positions.append({
            'tag': q['tag'],
            'position': position
        })
        position += 1
    
    for q in current_positions:
        for q2 in future_positions:
            if q2['tag'] == q['tag']:
                change_per_coin.append({
                    'tag': q['tag'],
                    'original_position': q['position'],
                    'future_position': q2['position'],
                    'change': abs(q['position'] - q2['position'])
                })

    return change_per_coin

def calculate_ranking(crypto_data):
    crypto_data = sorted(crypto_data, key=lambda k: k['profitability'], reverse=True)
    
    positions = []
    position = 1
    
    for q in crypto_data:
        positions.append({
            'tag': q['tag'],
            'position': position
        })
        position += 1
        
    return positions
        
def generate_future_mining_data(coin_list, date_time, gpu_data, usd_exchange_rate):
    cryptometadata = mongo.db.cryptometadata
    future_results = []
    hashrate = 0

    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')
    
    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    
    for coin in coin_list:
        run_results = 0
        for q in cryptometadata.find({"tag": coin, "date_time_captured" : {"$gte": date_lower_range}}).limit(1440):
            for q2 in gpu_data:
                if q2['algorithm'] == q['algorithm']:
                    hashrate = q2['hashrate']
                    break
                elif q['algorithm'] == 'CryptoNightHeavy' and q2['algorithm'] == 'CNHeavy':
                    hashrate = q2['hashrate']
                    break
            run_results += profitability(usd_exchange_rate, hashrate, 60, q['block_time'], q['exchange_rate'], q['block_reward'], q['nethash'], q['difficulty'], q['difficulty24'])
        future_results.append({
            'tag': coin,
            'profitability': run_results
        })
    
    return future_results
    
def profitability(er, h, t, bt, ber, br, nh, d, d24):
    blocks_pd = t / float(bt)
    net_btc = blocks_pd * ber * br
    nethash24 = (nh / d) * d24
    return (net_btc * (er / nethash24)) * h

def save_pso_results(current_batch_id, pso_crypto_data, pso_future_data, pso_results, gpu, variant):
    psoresults = mongo.db.psoresults
    
    wtm_change = calculate_position_change(pso_crypto_data, pso_future_data, reverse=True)
    model_change = calculate_position_change(pso_results, pso_future_data, reverse=True)
    wtm_change_per_coin = calculate_change_per_coin(pso_crypto_data, pso_future_data)
    model_change_per_coin = calculate_change_per_coin(pso_results, pso_future_data)
    
    psoresults.insert({
        'batch_id': current_batch_id,
        'gpu': gpu,
        'variant': variant,
        'wtm_change': wtm_change,
        'model_change': model_change,
        'wtm_change_per_coin': wtm_change_per_coin,
        'model_change_per_coin': model_change_per_coin,
        'wtm_ranking': calculate_ranking(pso_crypto_data),
        'model_ranking': calculate_ranking(pso_results),
        'future_ranking': calculate_ranking(pso_future_data)
    })
                
def get_actual_crypto_data(tag, date_time, timesteps):
    cryptometadata = mongo.db.cryptometadata
    exchangemetadata = mongo.db.exchangemetadata
    results = []
    
    datesplit = date_time.split('Z')
    date = datesplit[0].split('-')
    time = datesplit[1].split(':')
    
    date_lower_range = datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    
    for q in cryptometadata.find({'tag':tag, 'date_time_captured':{'$gte':date_lower_range}}).limit(timesteps):
        exchange_rate = exchangemetadata.find_one({'batch_id':q['batch_id']})
        if exchange_rate == None:
            exchange_rate = {'exchange_rate' : 6400}
        q['usd_exchange_rate'] = exchange_rate['exchange_rate']
        results.append(q)
        
    return results

def calculate_mining(profitability_ranking, date_time, timesteps, order = True):
    profitability_ranking = sorted(profitability_ranking, key=lambda k: k['profitability'], reverse=order)
    
    actual_crypto_data = get_actual_crypto_data(profitability_ranking[0]['tag'], date_time, timesteps)
    profit = 0
    
    for i in range(timesteps):
        kwargs = {"er": actual_crypto_data[i]['usd_exchange_rate'], 
                  "h": profitability_ranking[0]['hashrate'], 
                  "t": 60, 
                  "bt":  actual_crypto_data[i]['block_time'],
                  "ber":  actual_crypto_data[i]['exchange_rate'],
                  "fber":  actual_crypto_data[i]['exchange_rate'],
                  "br":  actual_crypto_data[i]['block_reward'],
                  "nh":  actual_crypto_data[i]['nethash'],
                  "d":  actual_crypto_data[i]['difficulty'],
                  "fd":  actual_crypto_data[i]['difficulty'],
                  "d24":  actual_crypto_data[i]['difficulty24']}
        
        profit += profitability_future_result(**kwargs)
        
    return profit
    
def save_pso_results2(current_batch_id, model_mining_result, wtm_mining_result, timesteps, gpu, variant, date_time):
    psoresults2 = mongo.db.psoresults2
    
    psoresults2.insert({
        'batch_id': current_batch_id,
        'model_mining_result': model_mining_result,
        'wtm_mining_result': wtm_mining_result,
        'timesteps':timesteps,
        'gpu':gpu,
        'variant':variant,
        'date_time': date_time
    })

