from flask import Flask, jsonify, request, abort, send_file, Blueprint
from flask_pymongo import PyMongo
import dns

class Coin:
    def __init__(self, tag, 
                 algorithm, 
                 block_time, 
                 block_reward, 
                 block_reward24, 
                 difficulty, 
                 exchange_rate, 
                 exchange_rate24, 
                 exchange_rate_curr, 
                 btc_revenue, 
                 btc_revenue24,
                 last_block,
                 difficulty24,
                 nethash,
                 market_cap,
                 estimated_rewards,
                 estimated_rewards24,
                 profitability,
                 profitability24,
                 lagging,
                 batch_id):
        self.tag = tag
        self.algorithm = algorithm
        self.block_time = block_time
        self.block_reward = block_reward
        self.block_reward24 = block_reward24
        self.difficulty = difficulty
        self.exchange_rate = exchange_rate
        self.exchange_rate24 = exchange_rate24
        self.exchange_rate_curr = exchange_rate_curr
        self.btc_revenue = btc_revenue
        self.btc_revenue24 = btc_revenue24
        self.last_block = last_block
        self.difficulty24 = difficulty24
        self.nethash = nethash
        self.market_cap = market_cap
        self.estimated_rewards = estimated_rewards
        self.estimated_rewards24 = estimated_rewards24
        self.profitability = profitability
        self.profitability24 = profitability24
        self.lagging = lagging
        self.batch_id = batch_id

crypto_analysis = Blueprint('crypto_analysis', __name__)
data_storage = Blueprint('data_storage', __name__)
rnn = Blueprint('rnn', __name__)
pso = Blueprint('pso', __name__)

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'pso_db'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/pso_db'

#app.config['MONGO_DBNAME'] = 'cos700db'
#app.config['MONGO_URI'] = 'mongodb://cos700_user:RdDp0eb0rwCMhWLh@cos700db-shard-00-00-mvzia.mongodb.net:27017,cos700db-shard-00-01-mvzia.mongodb.net:27017,cos700db-shard-00-02-mvzia.mongodb.net:27017/test?ssl=true&replicaSet=cos700DB-shard-0&authSource=admin&retryWrites=true'

#MONGO Password RdDp0eb0rwCMhWLh

#mongodb+srv://cos700_user:<PASSWORD>@cos700db-mvzia.mongodb.net/test?retryWrites=true

mongo = PyMongo(app)

from .analysis import *
from .storage import *
from .rnn import *
from .pso import *