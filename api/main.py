from flask import Flask, jsonify, request, abort, send_file
from flask_pymongo import PyMongo
from pprint import pprint
from io import BytesIO
from datetime import datetime
import json
import matplotlib.pyplot as plt
import numpy as np

from controllers import *

app = Flask(__name__)

app.register_blueprint(crypto_analysis)
app.register_blueprint(data_storage)
app.register_blueprint(rnn)
app.register_blueprint(pso)

if __name__ == '__main__':
    app.run(debug=False)